export interface UserInterface {
    token: string;
    email: string;
    password: string
}
