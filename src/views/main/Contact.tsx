import BACK from "../../assets/images/back.svg";
import DROPDOWN from "../../assets/images/dropdown.png";
import SEND from "../../assets/images/send.svg";
import { useNavigate } from "react-router-dom";
import { Newmessage } from "../../components";
import { useState } from "react";

export const Contact = (): JSX.Element => {
  const [showDialog, setShowDialog] = useState(false);

  const handleShowDialog = () => {
    setShowDialog(true);
  };

  const handleHIdeDialog = () => {
    setShowDialog(false);
  };

  const navigate = useNavigate();

  return (
    <>
      <div className="contact">
        <div className="contact_container">
          <div className="contact_head">
            <div className="back">
              <img src={BACK} alt="" onClick={() => navigate(-1)} />
              <h1>Search Contacts</h1>
            </div>
            <div className="user">
              <img src={DROPDOWN} alt="" />
            </div>
          </div>
          <div className="keyword_result">
            <div className="keyword_head">
              <div className="keyword">
                <p>Results for keyword:</p>
                <h2> KEYWORD</h2>
              </div>
              <div className="msg">
                <button onClick={handleShowDialog}>Send Message</button>
              </div>
            </div>
            <div className="keyword_content">
              <table>
                <tr className="tr_default">
                  <th>
                    <input type="checkbox" />
                  </th>
                  <th>Website Address</th>
                  <th>Source URL</th>
                  <th>Date Updated</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
                <tr className="tr_2_default">
                  <td>
                    <input type="checkbox" />
                  </td>
                  <td>www.samplesite.com</td>
                  <td>https://www.youtube.com/watch?v=ByXCqJxD1f0</td>
                  <td>22/11/2022</td>
                  <td className="focus">active</td>
                  <td>
                    <img src={SEND} alt="" />
                  </td>
                </tr>
                <tr className="tr_3_default">
                  <td>
                    <input type="checkbox" />
                  </td>
                  <td>www.samplesite.com</td>
                  <td>https://www.youtube.com/watch?v=ByXCqJxD1f0</td>
                  <td>22/11/2022</td>
                  <td className="focus">Inactive</td>
                  <td>
                    <img src={SEND} alt="" />
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      {!!showDialog && (
        <Newmessage show={!!showDialog} onClose={handleHIdeDialog} />
      )}
    </>
  );
};
