import React from "react";
import SEARCH from "../../assets/images/search.png";
import SEARCH_ICON from "../../assets/images/gray_search.svg";
import DOWN from "../../assets/images/dropdown.png";
import {NavLink} from "react-router-dom";

export const Welcome = (): JSX.Element => {
  return (
    <>
      <div className="welcome">
        <div className="header_container">
          <div className="header_head">
            <h1>Welcome Username!</h1>
            <img src={DOWN} alt="" />
          </div>
        </div>
        <div className="welcome_container">
          <div className="welcome_head">
            <h1>Search for the websites to contact</h1>
          </div>
          <div className="search_img">
            <img src={SEARCH} alt="" />
          </div>
          <div className="search_contact">
            <div className="inputs">
              <div className="label">
                <label htmlFor="keyword">Search Contacts</label>
              </div>
              <div className="input_div">
                <div className="simple">
                  <input
                    type="text"
                    name="keyword"
                    id="keyword"
                    placeholder="Enter keyword"
                  />
                  <img src={SEARCH_ICON} alt="" />
                </div>
                <div className="search_btn">
                  <button><NavLink to='/feature-1'>Search</NavLink></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
