import React from "react";
import {NavLink, useNavigate} from "react-router-dom";
import BOTTOM from "../../assets/images/bottom_img.png";
import {useAuthContext} from "../../contexts";
import {UserInterface} from "../../interfaces";

export const Login = (): JSX.Element => {
  const context = useAuthContext()
  const navigate = useNavigate()
  const user:UserInterface = {
    token: 'hfakhguyfv24234v24jhg234gf43f',
    email: 'example@you.com',
    password: '11223344'
  }
  const handleLogin = () => {
    context.updateUser(user)
    navigate('/welcome')
  }
  return (
    <>
      <div className="login">
        <div className="auth_container">
          <div className="div1">
            <div className="h_tags">
              <h1>LOGO</h1>
              <h2>Register now to boost your business growth!</h2>
            </div>
          </div>
          <div className="div2">
            <div className="div3">
              <div className="login_head">
                <h1>Log In</h1>
              </div>
              <form className="login_form">
                <div className="inputs">
                  <div className="div">
                    <label htmlFor="email">Work Email Address *</label>
                    <input
                      type="email"
                      name="email"
                      id="email"
                      className="input"
                      placeholder="johndoe@example.com"
                    />
                  </div>
                  <div className="div">
                    <label htmlFor="password">Password *</label>
                    <input
                      type="password"
                      name="password"
                      id="password"
                      className="input"
                      placeholder="*********"
                    />
                  </div>
                </div>
                <div className="forgot">
                  <button>Forgot Password?</button>
                </div>
                <div className="login_btn">
                  <button onClick={handleLogin}>Login</button>
                </div>
              </form>
              <div className="another_acc">
                <p>
                  Don’t have an account?{" "}
                  <NavLink to="/sign-up"> Sign Up</NavLink>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="bottom_img">
          <img src={BOTTOM} alt="" />
        </div>
      </div>
    </>
  );
};
