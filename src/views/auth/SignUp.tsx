import React from "react";
import { NavLink } from "react-router-dom";
import BOTTOM from "../../assets/images/bottom_img.png";

export const SignUp = (): JSX.Element => {
  return (
    <>
      <div className="signup">
        <div className="auth_container">
          <div className="div1">
            <div className="h_tags">
              <h1>LOGO</h1>
              <h2>Register now to boost your business growth!</h2>
            </div>
          </div>
          <div className="div2">
            <div className="signup_head">
              <h1>Sign Up</h1>
            </div>
            <form className="signup_form">
              <div className="inputs">
                <div className="div">
                  <label htmlFor="fullname">Full Name *</label>
                  <input
                    type="text"
                    name="fullname"
                    id="fullname"
                    className="input"
                    placeholder="John"
                  />
                </div>
                <div className="div">
                  <label htmlFor="password">Password *</label>
                  <input
                    type="password"
                    name="password"
                    id="password"
                    className="input"
                    placeholder="*********"
                  />
                </div>
                <div className="div">
                  <label htmlFor="email">Work Email Address *</label>
                  <input
                    type="email"
                    name="email"
                    id="email"
                    className="input"
                    placeholder="johndoe@example.com"
                  />
                </div>
                <div className="div">
                  <label htmlFor="password">Confirm Password *</label>
                  <input
                    type="password"
                    name="password"
                    id="password"
                    className="input"
                    placeholder="*********"
                  />
                </div>
              </div>
              <div className="create_acc">
                <button>Create Account</button>
              </div>
              <div className="already_log_in">
                <p>
                  Already have an account?{" "}
                  <NavLink to="/log-in"> Log In</NavLink>
                </p>
              </div>
            </form>
          </div>
        </div>
        <div className="bottom_img">
          <img src={BOTTOM} alt="" />
        </div>
      </div>
    </>
  );
};
