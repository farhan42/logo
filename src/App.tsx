import React from "react";
import { AuthRoutes } from "./layouts/auth";
import {useAuthContext} from "./contexts";
import MainRoutes from "./layouts/main/MainRoutes";

function App() {
    const context = useAuthContext()
  return (
    <>
        {context.isAuthenticated ? <MainRoutes /> : <AuthRoutes />}
    </>
  );
}

export default App;
