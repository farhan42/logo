import React from "react";
import FEATURES_1 from "../assets/images/features.svg";
import {useAuthContext} from "../contexts";
import {useNavigate} from "react-router-dom";

export const Sidebar = (): JSX.Element => {
  const context = useAuthContext()
  const navigate = useNavigate()
  const handleLogout = () => {
    context.updateUser(null)
    navigate('log-in')
  }
  return (
    <>
      <div className="nav">
        <div className="nav_container">
          <div className="nav_head">
            <h1>LOGO</h1>
          </div>
          <div className="nav_links">

            {/* <NavLink to={'/search-contact'}>
            <div className="link">
              <img src={SEARCH} alt="" />
              Search Websites
            </div>
            </NavLink> */}

            <div className="link">
              <img src={FEATURES_1} alt="" />
              Feature 1
            </div>



          </div>

        </div>
        <button className='logout-btn' onClick={handleLogout}>
          Logout
        </button>
      </div>
    </>
  );
};
