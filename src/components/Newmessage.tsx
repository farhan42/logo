import React from "react";
import { Modal } from "react-bootstrap";
import REMOVE from "../assets/images/message_remove.svg";

interface Props {
  show: boolean;
  onClose: () => void;
}

export const Newmessage = (props: Props): JSX.Element => {
  return (
    <div className="message">
      <div className="message_container">
        <Modal
          show={props.show}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <div className="logo_message">
            <div className="message_head">
              <h1>Send Message</h1>
              <img onClick={props.onClose} src={REMOVE} alt="" />
            </div>
            <div className="message_content">
              <div className="message_inputs">
                <div className="inputs">
                  <div className="input">
                    <label htmlFor="title">Message Title/Subject</label>
                    <input
                      type="text"
                      name="title"
                      id="title"
                      placeholder="Sample Text"
                    />
                  </div>
                  <div className="input">
                    <label htmlFor="template">Message Template</label>
                    <textarea
                      name="template"
                      id="template"
                      cols={30}
                      rows={10}
                      placeholder="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. "
                    ></textarea>
                  </div>
                  <div className="message_btns">
                    <button className="save" onClick={props.onClose}>
                      Save
                    </button>
                    <button className="send">Send Message</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>
      </div>
    </div>
  );
};
