import React from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { Contact, Welcome } from "../../views/main";
import { Mainlayout } from "./Mainlayout";

const MainRoutes = () => {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Mainlayout />}>
            <Route path="/welcome" element={<Welcome />} />
            <Route path="/feature-1" element={<Contact />} />
            <Route path="/" element={<Navigate to="/welcome" />}/>
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default MainRoutes;
