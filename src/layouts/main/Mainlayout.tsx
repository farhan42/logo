import React from "react";
import { Outlet } from "react-router-dom";
import { Sidebar } from "../../components";

export const Mainlayout = (): JSX.Element => {
  return (
    <div className="mainlayout">
      <div className="mainlayout_container">
        <div className="sidebar">
          <Sidebar />
        </div>
        <div className="outlet">
          <Outlet />
        </div>
      </div>
    </div>
  );
};
