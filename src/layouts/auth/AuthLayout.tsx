import React from "react";
import { Outlet } from "react-router-dom";

export const Authlayout = (): JSX.Element => {
  return (
    <div className="auth_container">
      <div className="authlayout">
        <Outlet />
      </div>
    </div>
  );
};
