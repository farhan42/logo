import React from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { Login, SignUp } from "../../views/auth";
import { Authlayout } from "./AuthLayout";

export const AuthRoutes = (): JSX.Element => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Authlayout />}>
          <Route path="/sign-up" element={<SignUp />} />
          <Route path="/log-in" element={<Login />} />
          <Route path="/" element={<Navigate to="/sign-up" />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};
